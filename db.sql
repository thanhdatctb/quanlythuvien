﻿use master
go
if (exists(select * from sysdatabases where name = 'QuanLyThuVien'))
drop database QuanLyThuVien
go
create database QuanLyThuVien
go 
use QuanLyThuVien
go
create table category
(
	id int primary key identity,
	name nvarchar(100),
	status bit default 1,
)
go
create table book
(
	id int primary key identity,
	name nvarchar(100),
	categoryid int foreign key references category(id),
	author nvarchar(100),
	status bit default 1,
)
go
create table reader
(
	id int primary key identity,
	name nvarchar(100),
	username nvarchar(100),
	hashpass nvarchar(100),
	status bit default 1,
)
go
create table rent
(
	id int primary key identity,
	rentDate datetime,
	paybackDate datetime,
	reader int foreign key references reader(id),
	book int foreign key references book(id)
)
go
create table admin
(
	id int primary key identity(1,1),
	userName varchar(100),
	hashpass varchar(100),
	adminName nvarchar(100)
)
go
---username: admin, password: admin, hash: sha1
insert into admin (userName,hashpass,adminName)values 
( 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin'),
('admin2', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin2')
go
insert into category (name) values
(N'Trinh thám'), (N'Tiểu thuyết'), (N'Truyện kinh dị')
go
insert into reader (name, username, hashpass) values
(N'Nguyễn Văn A', 'anv','d033e22ae348aeb5660fc2140aec35850c4da997'),
(N'Trần Văn B', 'btv', 'd033e22ae348aeb5660fc2140aec35850c4da997')

select * from category