﻿using QuanLyThuVien.Controller;
using QuanLyThuVien.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.View
{
    public partial class Admin : Form
    {
        private Model1 db = new Model1();
        private CategoryController categoryController = new CategoryController();
        private ReaderController readerController = new ReaderController();
        private BookController bookController = new BookController();
        private RentController rentController = new RentController();
        private BindingSource categorySource = new BindingSource();
       
        public Admin()
        {
            InitializeComponent();
            Load();
            this.cbCategory.DataSource = this.categoryController.GetAll();
            this.cbCategory.DisplayMember = "name";
            this.cbCategory.ValueMember = "id";

            this.cbBook.DataSource = this.bookController.GetAll();
            this.cbBook.DisplayMember = "name";
            this.cbBook.ValueMember = "id";
        }
        private void LoadData()
        {
            this.dtgvReader.DataSource = readerController.GetAll();
            db.categories.Where(x => x.status == true).Load();
            categorySource.DataSource = db.categories.Local.ToBindingList();
            dtgvCategory.DataSource = categorySource;
           

            this.dtgvCategory.DataSource = categorySource;
            this.dtgvBook.DataSource = this.bookController.GetAll();
            this.dtgvRent.DataSource = this.rentController.GetAll();
            
        }
        private void Load()
        {
            LoadData();
            AddBinding();
        }    
        private void AddBinding()
        {
            txtReaderId.DataBindings.Add(new Binding("Text", dtgvReader.DataSource, "id", true, DataSourceUpdateMode.Never));
            this.txtReaderName.DataBindings.Add(new Binding("Text", dtgvReader.DataSource, "name", true, DataSourceUpdateMode.Never));

            this.txtReaderUserName.DataBindings.Add(new Binding("Text", dtgvReader.DataSource, "username", true, DataSourceUpdateMode.Never));
            this.txtCategoryId.DataBindings.Add(new Binding("Text", dtgvCategory.DataSource, "id", true, DataSourceUpdateMode.Never));
            this.txtCategoryName.DataBindings.Add(new Binding("Text", dtgvCategory.DataSource, "Name", true, DataSourceUpdateMode.Never));

            this.txtBookId.DataBindings.Add(new Binding("Text", dtgvBook.DataSource, "id", true, DataSourceUpdateMode.Never));
            this.txtBookName.DataBindings.Add(new Binding("Text", dtgvBook.DataSource, "name", true, DataSourceUpdateMode.Never));
            this.cbCategory.DataBindings.Add(new Binding("SelectedValue", dtgvBook.DataSource, "categoryid", true, DataSourceUpdateMode.Never));

            this.cbBook.DataBindings.Add(new Binding("SelectedValue", dtgvRent.DataSource, "book", true, DataSourceUpdateMode.Never));
            this.txtRentId.DataBindings.Add(new Binding("Text", dtgvRent.DataSource, "id", true, DataSourceUpdateMode.Never));
            this.txtRentReaderId.DataBindings.Add(new Binding("Text", dtgvRent.DataSource, "reader", true, DataSourceUpdateMode.Never));
            this.dpRentDate.DataBindings.Add(new Binding("Value", dtgvRent.DataSource, "rentDate", true, DataSourceUpdateMode.Never));
            this.dpPaybackDate.DataBindings.Add(new Binding("Value", dtgvRent.DataSource, "paybackDate", true, DataSourceUpdateMode.Never));

        }

        private void btnAddMenu_Click(object sender, EventArgs e)
        {
            category category = new category();
            category.status = true;
            category.name = this.txtCategoryName.Text;
            ;
            if(categoryController.Add(category))
            {
                MessageBox.Show("Thêm danh mục thành công");
            }
            
            new Admin().Show();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtCategoryId.Text);
            if (categoryController.Delete(id))
            {
                MessageBox.Show("Xóa danh mục thành công");
                new Admin().Show();
            }
            
        }

        private void btnEditMenu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtCategoryId.Text);
            string name = this.txtCategoryName.Text;
            category category = new category();
            category.id = id;
            category.name = name;
            category.status = true;
            if(categoryController.Edit(category))
            {
                MessageBox.Show("Sửa danh mục thành công");
                new Admin().Show();
            }    
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'quanLyThuVienDataSet.category' table. You can move, or remove it, as needed.
            this.categoryTableAdapter.Fill(this.quanLyThuVienDataSet.category);

        }

        private void categoryBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.categoryTableAdapter.FillBy(this.quanLyThuVienDataSet.category);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillBy1ToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.categoryTableAdapter.FillBy1(this.quanLyThuVienDataSet.category);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtReaderId.Text);
            string name = this.txtReaderName.Text;
            string userName = this.txtReaderUserName.Text;
            reader reader = new reader();
            reader.name = name;
            reader.status = true;
            reader.username = userName;
            if(readerController.Add(reader))
            {
                MessageBox.Show("Thêm độc giả thành công");
                new Admin().Show();
            }
        }

        private void Admin_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'quanLyThuVienDataSet1.reader' table. You can move, or remove it, as needed.
            this.readerTableAdapter.Fill(this.quanLyThuVienDataSet1.reader);

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtReaderId.Text);
            string name = this.txtReaderName.Text;
            string userName = this.txtReaderUserName.Text;
            reader reader = new reader();
            reader.name = name;
            reader.id = id;
            reader.status = true;
            reader.username = userName;
            if (readerController.Edit(reader))
            {
                MessageBox.Show("Sửa độc giả thành công");
                new Admin().Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtReaderId.Text);
            string name = this.txtReaderName.Text;
            string userName = this.txtReaderUserName.Text;
            reader reader = new reader();
            reader.name = name;
            reader.id = id;
            reader.status = true;
            reader.username = userName;
            if (readerController.Delete(id))
            {
                MessageBox.Show("Xóa độc giả thành công");
                new Admin().Show();
            }
        }

        private void Admin_Load_2(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'quanLyThuVienDataSet1.category' table. You can move, or remove it, as needed.
            this.categoryTableAdapter1.Fill(this.quanLyThuVienDataSet1.category);

        }

        private void fillByToolStripButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.categoryTableAdapter1.FillBy(this.quanLyThuVienDataSet1.category);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
           // int id = int.Parse(this.txtBookId.Text);
            string name = this.txtBookName.Text;
            int catid = int.Parse(this.cbCategory.SelectedValue.ToString());
            string author = this.txtAuthor.Text;
            book book = new book();
            book.status = true;
            book.name = name;
            book.categoryid = catid;
            book.author = author;
            if(this.bookController.Add(book))
            {
                MessageBox.Show("Thêm sách thành công");
                new Admin().Show();
            }    
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtBookId.Text);
            string name = this.txtBookName.Text;
            int catid = int.Parse(this.cbCategory.SelectedValue.ToString());
            string author = this.txtAuthor.Text;
            book book = new book();
            book.status = true;
            book.name = name;
            book.categoryid = catid;
            book.author = author;
            book.id = id;
            if (this.bookController.Edit(book))
            {
                MessageBox.Show("Sửa sách thành công");
                new Admin().Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
             int id = int.Parse(this.txtBookId.Text);
            string name = this.txtBookName.Text;
            int catid = int.Parse(this.cbCategory.SelectedValue.ToString());
            string author = this.txtAuthor.Text;
            book book = new book();
            book.status = true;
            book.name = name;
            book.categoryid = catid;
            book.author = author;
            if (this.bookController.Delete(id))
            {
                MessageBox.Show("Xóa sách thành công");
                new Admin().Show();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
           
            try
            {
                int bookid = int.Parse(this.cbBook.SelectedValue.ToString());
                int readerId = int.Parse(this.txtRentReaderId.Text);
                DateTime rentDate = this.dpRentDate.Value;
                DateTime paybackDate = this.dpPaybackDate.Value;

                rent rent = new rent();
                rent.book = bookid;
                rent.reader = readerId;
                rent.rentDate = rentDate;
                rent.paybackDate = paybackDate;
                rentController.Add(rent);
                MessageBox.Show("Đã thêm thông tin thuê sách");
                new Admin().Show();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                new Admin().Show();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            int rentId = int.Parse(this.txtRentId.Text);
            int bookid = int.Parse(this.cbBook.SelectedValue.ToString());
            int readerId = int.Parse(this.txtRentReaderId.Text);
            DateTime rentDate = this.dpRentDate.Value;
            DateTime paybackDate = this.dpPaybackDate.Value;

            rent rent = new rent();
            rent.book = bookid;
            rent.reader = readerId;
            rent.rentDate = rentDate;
            rent.paybackDate = paybackDate;
            rent.id = rentId;
            try
            {
                rentController.Edit(rent);
                MessageBox.Show("Đã Sửa thông tin thuê sách");
                new Admin().Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
