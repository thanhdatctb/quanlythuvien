﻿using QuanLyThuVien.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.View
{
    public partial class LogIn : Form
    {
        private AdminController adminController = new AdminController();
        public LogIn()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string hashpass = PasswordController.Sha1Hash(txtPassword.Text);
            if(adminController.LogIn(username,hashpass))
            {
                new Admin().Show();
            }   else
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
            }    
        }
    }
}
