namespace QuanLyThuVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("admin")]
    public partial class admin
    {
        public int id { get; set; }

        [StringLength(100)]
        public string userName { get; set; }

        [StringLength(100)]
        public string hashpass { get; set; }

        [StringLength(100)]
        public string adminName { get; set; }
    }
}
