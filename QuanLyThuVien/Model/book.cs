namespace QuanLyThuVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("book")]
    public partial class book
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public book()
        {
            rents = new HashSet<rent>();
        }

        public int id { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        public int? categoryid { get; set; }
        public string categoryName
        {
            get
            {
                return category.name;
            }
        }
        [StringLength(100)]
        public string author { get; set; }

        public bool? status { get; set; }

        public virtual category category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rent> rents { get; set; }
       
    }
}
