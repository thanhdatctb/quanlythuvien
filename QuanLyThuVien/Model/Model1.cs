namespace QuanLyThuVien.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model11")
        {
        }

        public virtual DbSet<admin> admins { get; set; }
        public virtual DbSet<book> books { get; set; }
        public virtual DbSet<category> categories { get; set; }
        public virtual DbSet<reader> readers { get; set; }
        public virtual DbSet<rent> rents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<admin>()
                .Property(e => e.userName)
                .IsUnicode(false);

            modelBuilder.Entity<admin>()
                .Property(e => e.hashpass)
                .IsUnicode(false);

            modelBuilder.Entity<book>()
                .HasMany(e => e.rents)
                .WithOptional(e => e.book1)
                .HasForeignKey(e => e.book);

            modelBuilder.Entity<reader>()
                .HasMany(e => e.rents)
                .WithOptional(e => e.reader1)
                .HasForeignKey(e => e.reader);
        }
    }
}
