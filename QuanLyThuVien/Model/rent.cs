namespace QuanLyThuVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.InteropServices.WindowsRuntime;

    [Table("rent")]
    public partial class rent
    {
        public int id { get; set; }

        public DateTime? rentDate { get; set; }

        public DateTime? paybackDate { get; set; }

        public int? reader { get; set; }

        public int? book { get; set; }

        public virtual book book1 { get; set; }

        public virtual reader reader1 { get; set; }

        public string bookName
        {
            get
            {
                return book1.name;
            }
        }
        public string readerName
        {
            get
            {
                return reader1.name;
            }
        }
    }
}
