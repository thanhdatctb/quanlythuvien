﻿using QuanLyThuVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.Controller
{
    class RentController
    {
        private Model1 db = new Model1();
        public List<rent> GetAll()
        {
            return db.rents.ToList();
        }
        public bool Add(rent category)
        {
            try
            {
                this.db.rents.Add(category);
                db.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            

        }
        public bool Edit(rent reader)
        {
            this.db.rents.AddOrUpdate(reader);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            rent cat = this.db.rents.Find(id);
            //cat.status = false;
            db.rents.Remove(cat);
            //db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();
            
            return true;
        }
    }
}
