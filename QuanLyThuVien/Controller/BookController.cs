﻿using QuanLyThuVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.Controller
{
    class BookController
    {
        private Model1 db = new Model1();
        public List<book> GetAll()
        {
            return db.books.Where(reader => reader.status == true).ToList();
        }
        public bool Add(book category)
        {
            try
            {
                this.db.books.Add(category);
                db.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            

        }
        public bool Edit(book reader)
        {
            this.db.books.AddOrUpdate(reader);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            book cat = this.db.books.Find(id);
            cat.status = false;
            db.books.Add(cat);
            db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();
            
            return true;
        }
    }
}
