﻿using QuanLyThuVien.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVien.Controller
{
    class AdminController
    {
        Model1 db = new Model1();
        public bool LogIn(string username, string hashpass)
        {
           var admin =  db.admins.Where(s => s.userName == username).Where(s => s.hashpass == hashpass).FirstOrDefault();
            if(admin == null)
            {
                return false;
            }    
            return true;
        }
    }
}
