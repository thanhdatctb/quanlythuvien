﻿using QuanLyThuVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.Controller
{
    class ReaderController
    {
        private Model1 db = new Model1();
        public List<reader> GetAll()
        {
            return db.readers.Where(reader => reader.status == true).ToList();
        }
        public bool Add(reader category)
        {
            try
            {
                this.db.readers.Add(category);
                db.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            

        }
        public bool Edit(reader reader)
        {
            this.db.readers.AddOrUpdate(reader);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            reader cat = this.db.readers.Find(id);
            cat.status = false;
            db.readers.Add(cat);
            db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();
            
            return true;
        }
    }
}
