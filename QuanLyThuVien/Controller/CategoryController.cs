﻿using QuanLyThuVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.Controller
{
    class CategoryController
    {
        private Model1 db = new Model1();
        public List<category> GetAll()
        {
            return db.categories.Where(category => category.status == true).ToList();
        }
        public bool Add(category category)
        {
            try
            {
                this.db.categories.Add(category);
                db.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            

        }
        public bool Edit(category category)
        {
            this.db.categories.AddOrUpdate(category);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            category cat = this.db.categories.Find(id);
            cat.status = false;
            db.categories.Add(cat);
            db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();
            
            return true;
        }
    }
}
